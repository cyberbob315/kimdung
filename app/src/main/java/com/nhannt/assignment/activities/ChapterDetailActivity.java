package com.nhannt.assignment.activities;

import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.BackgroundColorSpan;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.miguelcatalan.materialsearchview.MaterialSearchView;
import com.nhannt.assignment.R;
import com.nhannt.assignment.SearchKeyWord;
import com.nhannt.assignment.base.BaseActivity;
import com.nhannt.assignment.database.DBQuery;
import com.nhannt.assignment.models.Chapter;
import com.nhannt.assignment.spellcheck.SpellChecker;
import com.nhannt.assignment.utils.StringUtils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChapterDetailActivity extends BaseActivity implements View.OnClickListener {

    public static final String KEY_CHAPTER_ID = "key_chapter_id";
    public static final String KEY_CHAPTER_POS = "key_chapter_pos";
    public static final String KEY_IS_SEARCH_RESULT = "key_is_search_result";
    public static final String KEY_SEARCH_KEY = "key_search_key";

    @BindView(R.id.toolbar)
    protected Toolbar mToolbar;
    @BindView(R.id.tv_chapter_content)
    protected AppCompatTextView tvChapterContent;
    @BindView(R.id.img_btn_next)
    protected ImageButton btnNextChapter;
    @BindView(R.id.img_btn_prev)
    protected ImageButton btnPrevChapter;
    @BindView(R.id.img_btn_share)
    protected ImageButton btnShare;
    @BindView(R.id.img_btn_spell_check)
    protected ImageButton btnSpellCheck;
    @BindView(R.id.ll_control)
    protected LinearLayout llControl;
    @BindView(R.id.nested_scroll)
    protected NestedScrollView nestedScrollView;
    @BindView(R.id.mProgressBar)
    protected ProgressBar mProgressBar;

    private Chapter chapter;
    private DBQuery dbQuery = DBQuery.getInstance(this);
    private SpannableString contentHighLight;
    private boolean isFromSearchResult = false;
    private String searchKeyWord = "";
    private int chapterPos;
    private int chapterId;
    private ArrayList<Chapter> lstChapter;
    private int lastScrollY = 0;
    private GetChapterContent asyncGetContent;
    private CheckSpell asyncCheckSpell;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chapter_detail);
        ButterKnife.bind(this);

        initControl();
        initEvents();
        getData();
    }

    private void initEvents() {
        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                search(query);
                return false;
            }


            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        nestedScrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {
                int scrollY = nestedScrollView.getScrollY();
                if (scrollY > lastScrollY) {
                    llControl.setVisibility(View.GONE);
                } else {
                    llControl.setVisibility(View.VISIBLE);
                }
                lastScrollY = scrollY;
            }
        });

        btnNextChapter.setOnClickListener(this);
        btnPrevChapter.setOnClickListener(this);
        btnShare.setOnClickListener(this);
        btnSpellCheck.setOnClickListener(this);

    }

    private void search(String query) {
        removeHighLight();
        String[] keywordArr = StringUtils.splitString(query);
        SearchKeyWord searchKeyWord = new SearchKeyWord();
        if (searchKeyWord.search(StringUtils.convertHtmlToString(chapter.getContent()), query)) {
            removeHighLight();
            for (String keyword : keywordArr) {
                highlightText(keyword, Color.YELLOW);
            }
        } else {
            if (!isFromSearchResult)
                showNotiDialog(getString(R.string.no_result));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.action_remove_high_light:
                removeHighLight();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void initControl() {
        mToolbar.setOverflowIcon(ContextCompat.getDrawable(this, R.drawable.ic_more_vert_white_24dp));
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp);
        }
        searchView = (MaterialSearchView) findViewById(R.id.search_view);
    }

    private void getData() {
        chapterId = getIntent().getIntExtra(KEY_CHAPTER_ID, -1);
        chapterPos = getIntent().getIntExtra(KEY_CHAPTER_POS, -1);

        isFromSearchResult = getIntent().getBooleanExtra(KEY_IS_SEARCH_RESULT, false);
        if (isFromSearchResult) {
            searchKeyWord = getIntent().getStringExtra(KEY_SEARCH_KEY);
        }

        asyncGetContent = new GetChapterContent();
        asyncGetContent.execute(chapterId);

    }

    @Override
    protected int getLayout() {
        return R.layout.activity_chapter_detail;
    }

    @Override
    protected int getMenuLayoutId() {
        return R.menu.chapter_detail_menu;
    }

    @Override
    protected void triggerSearchView(String keyword) {
        if (keyword == null)
            searchView.showSearch();
        else
            search(keyword);
    }

    @Override
    protected void triggerSpellCheck() {
        asyncCheckSpell = new CheckSpell();
        asyncCheckSpell.execute();
    }

    @Override
    public void onBackPressed() {
        if (asyncCheckSpell != null)
            asyncCheckSpell.cancel(true);
        if (asyncGetContent != null)
            asyncGetContent.cancel(true);
        super.onBackPressed();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_btn_next:
                if (chapterPos == (lstChapter.size() - 1))
                    chapterPos = 0;
                else
                    chapterPos++;
                chapter = lstChapter.get(chapterPos);
                updateInfo();
                break;
            case R.id.img_btn_prev:
                if (chapterPos == 0)
                    chapterPos = lstChapter.size() - 1;
                else
                    chapterPos--;
                chapter = lstChapter.get(chapterPos);
                updateInfo();
                break;
            case R.id.img_btn_share:
                share(chapter.getTitle(), chapter.getContent());
                break;
            case R.id.img_btn_spell_check:
                triggerSpellCheck();
                break;
        }
    }

    private class CheckSpell extends AsyncTask<Void, Void, Void> {
        int numberMistakes = 0;
        ArrayList<String> errorList = new ArrayList<>();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            removeHighLight();
            contentHighLight = new SpannableString(tvChapterContent.getText());
            showProgressDialog(getString(R.string.spell_checking));
        }

        @Override
        protected Void doInBackground(Void... params) {
            String content;
            SpellChecker spellChecker = SpellChecker.getInstance();
            content = StringUtils.convertHtmlToString(chapter.getContent());
            //Check spell for each word,if there is any mistake,that word will be highlighted
            String[] wordArr = StringUtils.splitString(content);
            for (String word : wordArr) {
                if (!spellChecker.check(word)) {
                    numberMistakes++;
                    Log.d("mistake", word);
                    errorList.add(word);
                    highlightText(contentHighLight, word, Color.GREEN);
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            hideProgressDialog();

            //High light mistakes founded
            tvChapterContent.setText(contentHighLight);

            //Show result dialog
            StringBuilder builder = new StringBuilder();
            builder.append("Tìm thấy " + numberMistakes + " lỗi\n\n");
            for (String word : errorList) {
                builder.append("-");
                builder.append(word);
                builder.append("\n");
            }
            showNotiDialog(builder.toString());
        }
    }


    private class GetChapterContent extends AsyncTask<Integer, Void, Void> {

        @Override
        protected void onPreExecute() {
            mProgressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Integer... params) {
            chapter = dbQuery.getChapterById(params[0]);
            lstChapter = dbQuery.getChaptersByBookId(chapter.getBookId());
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            updateInfo();
            //If user start from search result activity
            if (isFromSearchResult)
                search(searchKeyWord);
            mProgressBar.setVisibility(View.GONE);
        }
    }

    private void updateInfo() {
        if (getSupportActionBar() != null)
            getSupportActionBar().setTitle(chapter.getTitle());
        tvChapterContent.setText(StringUtils.convertHtmlToString(chapter.getContent()));
    }


    private void removeHighLight() {
        SpannableString spannableString = new SpannableString(tvChapterContent.getText());
        BackgroundColorSpan[] backgroundColorSpan =
                spannableString.getSpans(0, spannableString.length(), BackgroundColorSpan.class);
        for (BackgroundColorSpan bgSpan : backgroundColorSpan) {
            spannableString.removeSpan(bgSpan);
        }
        tvChapterContent.setText(spannableString);
    }


    private void highlightText(String s, int color) {
        SpannableString spannableString = new SpannableString(tvChapterContent.getText());
        int indexOfKeyWord = spannableString.toString().indexOf(s);
        while (indexOfKeyWord > 0) {
            spannableString.setSpan(new BackgroundColorSpan(color), indexOfKeyWord,
                    indexOfKeyWord + s.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            indexOfKeyWord = spannableString.toString().indexOf(s, indexOfKeyWord + s.length());
        }
        tvChapterContent.setText(spannableString);
    }

    private void highlightText(SpannableString spannableString, String s, int color) {
        int indexOfKeyWord = spannableString.toString().indexOf(s);
        while (indexOfKeyWord > 0) {
            spannableString.setSpan(new BackgroundColorSpan(color), indexOfKeyWord,
                    indexOfKeyWord + s.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            indexOfKeyWord = spannableString.toString().indexOf(s, indexOfKeyWord + s.length());
        }
    }


}
