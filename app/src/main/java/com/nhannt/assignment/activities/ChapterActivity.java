package com.nhannt.assignment.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.miguelcatalan.materialsearchview.MaterialSearchView;
import com.nhannt.assignment.R;
import com.nhannt.assignment.adapters.ChapterAdapter;
import com.nhannt.assignment.base.BaseActivity;
import com.nhannt.assignment.database.DBQuery;
import com.nhannt.assignment.models.Book;
import com.nhannt.assignment.models.Chapter;
import com.nhannt.assignment.spellcheck.SpellChecker;
import com.nhannt.assignment.utils.Common;
import com.nhannt.assignment.utils.StringUtils;
import com.nhannt.assignment.widgets.ItemOffsetDecoration;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChapterActivity extends BaseActivity implements AppBarLayout.OnOffsetChangedListener {

    public static final String KEY_TRANSITION_NAME = "transition_key";
    public static final String KEY_BOOK = "book_key";
    public static final String KEY_IS_USE_TRANSITION = "use_transition_key";

    @BindView(R.id.toolbar)
    protected Toolbar toolbar;
    @BindView(R.id.iv_book_cover_chapter)
    protected ImageView ivBookCover;
    @BindView(R.id.chapter_collaspe_toolbar)
    protected CollapsingToolbarLayout collapsingToolbarLayout;
    @BindView(R.id.rv_chapter_list)
    protected RecyclerView rvChapterList;
    @BindView(R.id.appbar_chapter)
    protected AppBarLayout appBarLayout;

    private String transitionName = "";
    private DBQuery dbQuery = DBQuery.getInstance(this);

    private Book itemBook;
    private int idBook;
    private int scrollRange = -1;
    private ArrayList<Chapter> lstChapter;
    private ChapterAdapter mChapterAdapter;
    private int numberMistakes = 0;
    private int spellCheckProgress = 0;
    private int doneCheckingAsyncCount = 0;
    private int numberAsyncExecuted = 0;
    private SpellChecker spellChecker = SpellChecker.getInstance();
    private ArrayList<String> lstSpellErrors;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chapter);
        ButterKnife.bind(this);
        getDataFromIntent();
        initControls();
        initEvents();

    }

    private void setCollapsingToolbarLayoutTitle() {
        collapsingToolbarLayout.setTitle(itemBook.getTitle());
        collapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.ExpandedAppBar);
        collapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.CollapsedAppBar);
        collapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.ExpandedAppBarPlus1);
        collapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.CollapsedAppBarPlus1);
    }

    private void getDataFromIntent() {
        Intent intent = getIntent();

        idBook = intent.getIntExtra(KEY_BOOK, -1);
        Log.d("bookid", idBook + "");
        boolean isTransition = intent.getBooleanExtra(KEY_IS_USE_TRANSITION, false);
        if (Common.isLolipop() && isTransition) {
            transitionName = intent.getStringExtra(KEY_TRANSITION_NAME);
            ivBookCover.setTransitionName(transitionName);
        }
        new GetBook().execute(idBook);
        new GetChapterList().execute(idBook);

    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
        //Initialize the size of the scroll
        if (scrollRange == -1) {
            scrollRange = appBarLayout.getTotalScrollRange();
        }
        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        //Check if the view is collapsed
        if (scrollRange + verticalOffset == 0) {
            toolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
            if (Common.isLolipop())
                window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
        } else {
            toolbar.setBackgroundColor(0);
            if (Common.isLolipop())
                window.setStatusBarColor(0);
        }
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_chapter;
    }

    @Override
    protected int getMenuLayoutId() {
        return R.menu.chapter_menu;
    }

    private class GetChapterList extends AsyncTask<Integer, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Integer... params) {
            lstChapter = dbQuery.getChaptersByBookId(params[0]);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            mChapterAdapter = new ChapterAdapter(ChapterActivity.this, lstChapter);
            rvChapterList.setAdapter(mChapterAdapter);
            mChapterAdapter.notifyDataSetChanged();
            super.onPostExecute(aVoid);
        }
    }

    private class GetBook extends AsyncTask<Integer, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Integer... params) {
            itemBook = dbQuery.getBookById(params[0]);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (itemBook != null) {
                int cover = Common.getResId(itemBook.getCover(), R.drawable.class);
                Glide.with(ChapterActivity.this)
                        .load(cover)
                        .placeholder(R.mipmap.ic_launcher)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(ivBookCover);
                setCollapsingToolbarLayoutTitle();
            }
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                supportFinishAfterTransition();
                return true;
            case R.id.action_share:
                share(itemBook.getTitle(), itemBook.getDescription());
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void triggerSearchView(String keyword) {
        if (keyword == null)
            searchView.showSearch();
        else
            navigateToSearchActivity(keyword);
    }

    @Override
    protected void triggerSpellCheck() {
        new GetAllChapter().execute();
    }

    private void navigateToSearchActivity(String keyword) {
        Intent intentToSearch = new Intent(ChapterActivity.this, SearchResultActivity.class);
        intentToSearch.putExtra(SearchResultActivity.KEY_SEARCH_TYPE, SearchResultActivity.SEARCH_SINGLE_BOOK);
        intentToSearch.putExtra(SearchResultActivity.KEY_SEARCH_KEY_WORD, keyword);
        intentToSearch.putExtra(SearchResultActivity.KEY_BOOK_ID, idBook);
        startActivity(intentToSearch);
    }


    private void initEvents() {
        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                navigateToSearchActivity(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        appBarLayout.addOnOffsetChangedListener(this);
        rvChapterList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            int scrollDy = 0;

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (scrollDy == 0 && (newState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE)) {
                    appBarLayout.setExpanded(true);
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                scrollDy += dy;
            }
        });
    }

    private void initControls() {
        toolbar.setOverflowIcon(ContextCompat.getDrawable(this, R.drawable.ic_more_vert_white_24dp));
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp);
        }

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(ChapterActivity.this, LinearLayoutManager.VERTICAL, false);
        rvChapterList.setLayoutManager(layoutManager);
        rvChapterList.addItemDecoration(new ItemOffsetDecoration(this, R.dimen.grid_spacing));
        searchView = (MaterialSearchView) findViewById(R.id.search_view);
    }

    private class GetAllChapter extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog(getString(R.string.loading));
        }

        List<Chapter> lstChapters;

        @Override
        protected Void doInBackground(Void... params) {
            //Get list chapter of book
            lstChapters = dbQuery.getChaptersByBookId(idBook);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            hideProgressDialog();
            lstSpellErrors = new ArrayList<>();
            //Split chapter list in to 3 parts
            List<List<Chapter>> lstArrChapters = Common.chopIntoParts(lstChapters, 3);
            numberAsyncExecuted = lstArrChapters.size();
            doneCheckingAsyncCount = 0;
            Toast.makeText(ChapterActivity.this, numberAsyncExecuted + "", Toast.LENGTH_SHORT).show();
            //Execute 3 spell check async task do check 3 parts parallely
            for (List<Chapter> lst : lstArrChapters) {
                ChapterActivity.AsyncSpellCheck asyncSpellCheck = new AsyncSpellCheck(lst);
                asyncSpellCheck.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }
            //Show checking progress dialog
            mProgressDialog = new ProgressDialog(ChapterActivity.this);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            mProgressDialog.setProgress(0);
            spellCheckProgress = 0;
            mProgressDialog.setMax(lstChapters.size());
            mProgressDialog.setTitle(getString(R.string.spell_checking));
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
        }
    }

    private class AsyncSpellCheck extends AsyncTask<Void, Void, Void> {

        List<Chapter> lstChapters;

        public AsyncSpellCheck(List<Chapter> lstChapters) {
            this.lstChapters = lstChapters;
        }

        @Override
        protected Void doInBackground(Void... params) {
            for (Chapter chapter : lstChapters) {
                String content = StringUtils.convertHtmlToString(chapter.getContent());
                String[] wordArr = StringUtils.splitString(content);
                for (String word : wordArr) {
                    if (!spellChecker.check(word)) {
                        numberMistakes++;
                        lstSpellErrors.add(word);
                        Log.d("mistake", word);
                    }
                }
                spellCheckProgress++;
                publishProgress();
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
            mProgressDialog.setProgress(spellCheckProgress);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            doneCheckingAsyncCount++;
            if (doneCheckingAsyncCount == numberAsyncExecuted) {
                mProgressDialog.hide();
                mProgressDialog.dismiss();
                //Show result in a alert dialog
                StringBuilder builder = new StringBuilder();
                builder.append("Phát hiện thấy " + numberMistakes + " lỗi!\n\n");
                for (String word : lstSpellErrors) {
                    builder.append("-");
                    builder.append(word);
                    builder.append("\n");
                }
                showNotiDialog(builder.toString());
            }
        }
    }
}
