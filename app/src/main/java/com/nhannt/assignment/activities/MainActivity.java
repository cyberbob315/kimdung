package com.nhannt.assignment.activities;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;

import com.miguelcatalan.materialsearchview.MaterialSearchView;
import com.nhannt.assignment.R;
import com.nhannt.assignment.adapters.BookAdapter;
import com.nhannt.assignment.base.BaseActivity;
import com.nhannt.assignment.database.DBQuery;
import com.nhannt.assignment.models.Book;
import com.nhannt.assignment.utils.Common;
import com.nhannt.assignment.widgets.ItemOffsetDecoration;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class MainActivity extends BaseActivity {
    private static final int PERMISSION_REQUEST_CODE = 200;

    @BindView(R.id.toolbar)
    protected Toolbar mToolbar;
    @BindView(R.id.rv_list_book)
    protected RecyclerView mRvListBook;

    private BookAdapter mBookAdapter;
    private ArrayList<Book> lstBook = new ArrayList<>();
    private DBQuery dbQuery = DBQuery.getInstance(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        initControl();
        initEvents();
        if (Common.isMarshMallow()) {
            if (!checkPermission()) {
                requestPermission();
            } else {
                doMainWork();
            }
        } else {
            doMainWork();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected int getMenuLayoutId() {
        return R.menu.home_menu;
    }

    private void initEvents() {

        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                navigateToSearchActivity(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

    }

    private void navigateToSearchActivity(String keyword) {
        Intent intentToSearch = new Intent(MainActivity.this, SearchResultActivity.class);
        intentToSearch.putExtra(SearchResultActivity.KEY_SEARCH_TYPE, SearchResultActivity.SEARCH_ALL_BOOK);
        intentToSearch.putExtra(SearchResultActivity.KEY_SEARCH_KEY_WORD, keyword);
        startActivity(intentToSearch);
    }


    private void doMainWork() {
        new LoadBook().execute();
    }

    private void initControl() {
        mToolbar.setOverflowIcon(ContextCompat.getDrawable(this, R.drawable.ic_more_vert_white_24dp));
        setSupportActionBar(mToolbar);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(MainActivity.this, 3, GridLayoutManager.VERTICAL, false);
        mRvListBook.setLayoutManager(layoutManager);
        ItemOffsetDecoration offsetDecoration = new ItemOffsetDecoration(this, R.dimen.grid_spacing);
        mRvListBook.addItemDecoration(offsetDecoration);
        searchView = (MaterialSearchView) findViewById(R.id.search_view);

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0) {
                    boolean writeAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean readAccepted = grantResults[1] == PackageManager.PERMISSION_GRANTED;

                    if (writeAccepted && readAccepted) {
                        doMainWork();
                    }
                }
        }
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_main;
    }

    @Override
    protected void triggerSearchView(String keyword) {
        if (keyword == null)
            searchView.showSearch();

        else {
            navigateToSearchActivity(keyword);
        }

    }

    @Override
    protected void triggerSpellCheck() {
    }

    private class LoadBook extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dbQuery.open();
            showProgressDialog(getString(R.string.loading));
        }

        @Override
        protected Void doInBackground(Void... params) {
            lstBook = dbQuery.getAllBook();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            mBookAdapter = new BookAdapter(MainActivity.this, lstBook);
            mRvListBook.setAdapter(mBookAdapter);
            mBookAdapter.notifyDataSetChanged();
            hideProgressDialog();
            super.onPostExecute(aVoid);
        }
    }

    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(getApplicationContext(), WRITE_EXTERNAL_STORAGE);
        int result1 = ContextCompat.checkSelfPermission(getApplicationContext(), READ_EXTERNAL_STORAGE);

        return result == PackageManager.PERMISSION_GRANTED && result1 == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{WRITE_EXTERNAL_STORAGE, READ_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        dbQuery.close();
    }
}
