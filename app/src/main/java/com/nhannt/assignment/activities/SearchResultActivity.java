package com.nhannt.assignment.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.nhannt.assignment.R;
import com.nhannt.assignment.SearchKeyWord;
import com.nhannt.assignment.adapters.SearchResultAdapter;
import com.nhannt.assignment.database.DBQuery;
import com.nhannt.assignment.models.Book;
import com.nhannt.assignment.models.Chapter;
import com.nhannt.assignment.utils.StringUtils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SearchResultActivity extends AppCompatActivity implements ExpandableListView.OnChildClickListener {

    public static final int SEARCH_ALL_BOOK = 1;
    public static final int SEARCH_SINGLE_BOOK = 2;
    public static final String KEY_SEARCH_TYPE = "search_type";
    public static final String KEY_BOOK_ID = "search_book_id";
    public static final String KEY_SEARCH_KEY_WORD = "search_key_word";

    @BindView(R.id.tv_search_result)
    protected TextView tvSearchResult;
    @BindView(R.id.expand_lv_search_result)
    protected ExpandableListView expandLvSearchResult;
    @BindView(R.id.progress_bar_search)
    protected ProgressBar progressBar;
    @BindView(R.id.tv_no_result)
    protected TextView tvNoResult;
    @BindView(R.id.toolbar)
    protected Toolbar mToolbar;

    private int searchType;
    private int bookId; // in case search single book
    private String searchKeyWord = "";
    private SearchResultAdapter mResultAdapter;
    private ArrayList<Book> lstBookResult = new ArrayList<>();
    private ArrayList<Chapter> lstChapterResult = new ArrayList<>();
    private DBQuery dbQuery = DBQuery.getInstance(this);
    private ProgressDialog mProgressDialog;
    private Search asyncSearch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_result);
        ButterKnife.bind(this);
        setupToolbar();
        getData();
        tvSearchResult.setText(getString(R.string.search_result, searchKeyWord));
        asyncSearch = new Search();
        asyncSearch.execute(searchKeyWord);
        expandLvSearchResult.setOnChildClickListener(this);

    }

    private void setupToolbar() {
        setSupportActionBar(mToolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowTitleEnabled(true);
            actionBar.setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp);
            actionBar.setTitle(getString(R.string.search));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (asyncSearch != null)
            asyncSearch.cancel(true);
        super.onBackPressed();
    }

    private void getData() {
        Intent intent = getIntent();
        searchType = intent.getIntExtra(KEY_SEARCH_TYPE, -1);
        if (searchType == SEARCH_SINGLE_BOOK) {
            bookId = intent.getIntExtra(KEY_BOOK_ID, -1);
        }
        searchKeyWord = intent.getStringExtra(KEY_SEARCH_KEY_WORD);
    }

    @Override
    public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
        Intent intent = new Intent(this, ChapterDetailActivity.class);
        intent.putExtra(ChapterDetailActivity.KEY_CHAPTER_ID, lstBookResult.get(groupPosition).getLstChapter().get(childPosition).getId());
        intent.putExtra(ChapterDetailActivity.KEY_IS_SEARCH_RESULT, true);
        intent.putExtra(ChapterDetailActivity.KEY_SEARCH_KEY, searchKeyWord);
        startActivity(intent);
        return false;
    }

    private class Search extends AsyncTask<String, Integer, Void> {

        int progress = 0;

        @Override
        protected void onPreExecute() {
            mProgressDialog = new ProgressDialog(SearchResultActivity.this);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
//            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setProgress(progress);
            mProgressDialog.setTitle(getString(R.string.searching));
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(String... params) {
            String keywords = params[0];
            SearchKeyWord searchKeyWord = new SearchKeyWord();
            ArrayList<Chapter> chapterIds;
            if (searchType == SEARCH_ALL_BOOK) {
                chapterIds = dbQuery.getAllChapter();
            } else {
                chapterIds = dbQuery.getChaptersByBookId(bookId);
            }
            mProgressDialog.setMax(chapterIds.size());
            String[] keywordArr = StringUtils.splitString(keywords);
            for (Chapter chapter : chapterIds) {
                progress++;
                publishProgress(progress);
                boolean isSuccess = false;
                if (keywordArr.length > 3) {
                    //If keyword is less than 3 words then use sqlite to search
                    isSuccess = searchKeyWord.search(StringUtils.convertHtmlToString(chapter.getContent()), keywords);
                } else {
                    //If keyword is more than 3 words use SearchKeyWord
                    isSuccess = dbQuery.searchInChapter(chapter.getId(), keywordArr);
                }
                if (isSuccess) {
                    lstChapterResult.add(chapter);
                }
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            mProgressDialog.setProgress(values[0]);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            new LoadExpandableListView().execute();
            mProgressDialog.dismiss();
            super.onPostExecute(aVoid);
        }
    }

    private class LoadExpandableListView extends AsyncTask<Void, Void, Void> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(Void... params) {
            for (Chapter chapter : lstChapterResult) {
                Book book = isBookIdExist(chapter.getBookId());
                if (book != null) {
                    book.addChapter(chapter);
                } else {
                    book = dbQuery.getBookById(chapter.getBookId(), false);
                    book.addChapter(chapter);
                    lstBookResult.add(book);
                }

            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            mResultAdapter = new SearchResultAdapter(SearchResultActivity.this, lstBookResult);
            expandLvSearchResult.setAdapter(mResultAdapter);
            progressBar.setVisibility(View.GONE);
            if (lstBookResult.isEmpty())
                tvNoResult.setVisibility(View.VISIBLE);
        }
    }

    private Book isBookIdExist(int bookId) {
        for (Book book : lstBookResult) {
            if (book.getId() == bookId)
                return book;
        }
        return null;
    }

}
