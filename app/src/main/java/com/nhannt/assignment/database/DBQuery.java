package com.nhannt.assignment.database;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.nhannt.assignment.models.Book;
import com.nhannt.assignment.models.Chapter;

import java.util.ArrayList;

/**
 * Created by NhanNT on 04/17/2017.
 */

public class DBQuery {

    private DBHelper dbHelper;
    private SQLiteDatabase db;
    private static DBQuery mInstance;

    public static synchronized DBQuery getInstance(Context context) {
        if (mInstance == null)
            mInstance = new DBQuery(context);
        return mInstance;
    }

    private DBQuery(Context context) {
        dbHelper = new DBHelper(context);
    }


    public DBQuery open() throws SQLException {
        try {
            dbHelper.openDataBase();
//            dbHelper.close();
            db = dbHelper.getReadableDatabase();
        } catch (SQLException ignored) {
        }
        return this;
    }

    public void close() {
        dbHelper.close();
    }

    public Book getBookById(int id) {
        StringBuilder builder = new StringBuilder();
        builder.append("SELECT * FROM " + DBHelper.TABLE_BOOK);
        builder.append(" WHERE " + DBHelper.COLUMN_BOOK_ID + " = " + id);
        Cursor result = db.rawQuery(builder.toString(), null);
        Book itemBook = null;
        try {
            if (result != null && result.moveToFirst()) {
                String title = result.getString(result.getColumnIndex(DBHelper.COLUMN_BOOK_TITLE));
                String cover = result.getString(result.getColumnIndex(DBHelper.COLUMN_BOOK_COVER)).trim();
                String description = result.getString(result.getColumnIndex(DBHelper.COLUMN_BOOK_DESCRIPTION));
                itemBook = new Book(id, title, cover, description);
            }
        } finally {
            if (result != null)
                result.close();
        }
        return itemBook;
    }

    public Chapter getChapterById(int chapterID) {
        Chapter chapter = null;
        StringBuilder builder = new StringBuilder();
        builder.append("SELECT * FROM " + DBHelper.TABLE_CHAPTER);
        builder.append(" WHERE " + DBHelper.COLUMN_CHAPTER_ID + " = " + chapterID);
        Cursor result = db.rawQuery(builder.toString(), null);
        try {
            if (result != null && result.moveToFirst()) {
                chapter = new Chapter();
                chapter.setId(result.getInt(result.getColumnIndex(DBHelper.COLUMN_CHAPTER_ID)));
                chapter.setTitle(result.getString(result.getColumnIndex(DBHelper.COLUMN_CHAPTER_TITLE)).trim());
                chapter.setBookId(result.getInt(result.getColumnIndex(DBHelper.COLUMN_BOOK_ID)));
                chapter.setContent(result.getString(result.getColumnIndex(DBHelper.COLUMN_CHAPTER_CONTENT)));
                chapter.setDate(result.getString(result.getColumnIndex(DBHelper.COLUMN_CHAPTER_DATE)));
                result.close();
            }
        } finally {
            if (result != null)
                result.close();
        }

        return chapter;
    }

    public ArrayList<Chapter> getChaptersByBookId(int bookId) {
        ArrayList<Chapter> lstChapter = new ArrayList<>();
        StringBuilder builder = new StringBuilder();
        builder.append(" SELECT * FROM " + DBHelper.TABLE_CHAPTER);
        builder.append(" WHERE " + DBHelper.COLUMN_CHAPTER_BOOK_ID + " = " + bookId);
        Cursor result = db.rawQuery(builder.toString(), null);
        try {

            if (result != null && result.moveToFirst()) {
                do {
                    Chapter chapter = new Chapter();
                    chapter.setId(result.getInt(result.getColumnIndex(DBHelper.COLUMN_CHAPTER_ID)));
                    chapter.setTitle(result.getString(result.getColumnIndex(DBHelper.COLUMN_CHAPTER_TITLE)).trim());
                    chapter.setBookId(bookId);
                    chapter.setContent(result.getString(result.getColumnIndex(DBHelper.COLUMN_CHAPTER_CONTENT)));
                    chapter.setDate(result.getString(result.getColumnIndex(DBHelper.COLUMN_CHAPTER_DATE)));
                    lstChapter.add(chapter);
                } while (result.moveToNext());
            }
        } finally {
            if (result != null)
                result.close();
        }
        return lstChapter;
    }

    public Book getBookById(int bookId, boolean isGetChapter) {
        Book book = new Book();
        book.setId(bookId);
        StringBuilder builder = new StringBuilder();
        builder.append("SELECT * FROM " + DBHelper.TABLE_BOOK);
        builder.append(" WHERE " + DBHelper.COLUMN_BOOK_ID + " = " + bookId);
        ArrayList<Chapter> lstChapter;
        if (isGetChapter) {
            lstChapter = getChaptersByBookId(bookId);
            book.setLstChapter(lstChapter);
        }
        Cursor result = db.rawQuery(builder.toString(), null);
        try {
            if (result != null && result.moveToFirst()) {
                book.setTitle(result.getString(result.getColumnIndex(DBHelper.COLUMN_BOOK_TITLE)));
                book.setCover(result.getString(result.getColumnIndex(DBHelper.COLUMN_BOOK_COVER)).trim());
                book.setDescription(result.getString(result.getColumnIndex(DBHelper.COLUMN_BOOK_DESCRIPTION)));
            }
        } finally {
            if (result != null)
                result.close();
        }

        return book;
    }

    public ArrayList<Integer> getAllChapterIdsByBook(int bookId) {
        ArrayList<Integer> chapterIds = new ArrayList<>();
        StringBuilder builder = new StringBuilder();
        builder.append("SELECT * FROM " + DBHelper.TABLE_CHAPTER);
        builder.append(" WHERE " + DBHelper.COLUMN_CHAPTER_BOOK_ID + " = " + bookId);
        Log.d("sql", builder.toString());
        Cursor result = db.rawQuery(builder.toString(), null);
        try {
            if (result != null && result.moveToFirst()) {
                int i = 0;
                do {
                    chapterIds.add(result.getInt(result.getColumnIndex(DBHelper.COLUMN_CHAPTER_ID)));
                } while (result.moveToNext());
            }
        } finally {
            if (result != null)
                result.close();
        }

        return chapterIds;
    }

    public ArrayList<Chapter> getAllChapter() {
        ArrayList<Chapter> lstChapter = new ArrayList<>();
        StringBuilder builder = new StringBuilder();
        builder.append("SELECT * FROM " + DBHelper.TABLE_CHAPTER);

        Cursor result = db.rawQuery(builder.toString(), null);
        try {
            if (result != null && result.moveToFirst()) {
                do {
                    Chapter chapter = new Chapter();
                    chapter.setBookId(result.getInt(result.getColumnIndex(DBHelper.COLUMN_CHAPTER_BOOK_ID)));
                    chapter.setId(result.getInt(result.getColumnIndex(DBHelper.COLUMN_CHAPTER_ID)));
                    chapter.setTitle(result.getString(result.getColumnIndex(DBHelper.COLUMN_CHAPTER_TITLE)).trim());
                    chapter.setContent(result.getString(result.getColumnIndex(DBHelper.COLUMN_CHAPTER_CONTENT)));
                    chapter.setDate(result.getString(result.getColumnIndex(DBHelper.COLUMN_CHAPTER_DATE)));
                    lstChapter.add(chapter);
                } while (result.moveToNext());
            }
        } finally {
            if (result != null)
                result.close();
        }

        return lstChapter;
    }

    public ArrayList<Integer> getAllChapterIds() {
        ArrayList<Integer> chapterIds = new ArrayList<>();
        StringBuilder builder = new StringBuilder();
        builder.append("SELECT * FROM " + DBHelper.TABLE_CHAPTER);
        Cursor result = db.rawQuery(builder.toString(), null);
        try {
            if (result != null && result.moveToFirst()) {
                int i = 0;
                do {
                    chapterIds.add(result.getInt(result.getColumnIndex(DBHelper.COLUMN_CHAPTER_ID)));
                } while (result.moveToNext());

            }
        } finally {
            if (result != null)
                result.close();
        }

        return chapterIds;
    }


    public boolean searchInChapter(int chapterId, String[] keyWord) {
        boolean isSuccess = false;
        StringBuilder builder = new StringBuilder();
        builder.append("SELECT * FROM " + DBHelper.TABLE_CHAPTER);
        builder.append(" WHERE " + DBHelper.COLUMN_CHAPTER_ID + " = " + chapterId);
        builder.append(" AND " + DBHelper.COLUMN_CHAPTER_CONTENT + " LIKE '");
        for (String s : keyWord) {
            builder.append("% " + s + " %");
        }
        builder.append("'");
        Cursor result = db.rawQuery(builder.toString(), null);
        try {

            if (result != null && result.moveToFirst()) {
                isSuccess = true;
            }
        } finally {
            if (result != null)
                result.close();
        }

        return isSuccess;
    }

    public ArrayList<Book> getAllBook() {
        ArrayList<Book> lstBook = new ArrayList<>();
        String sql = DBHelper.QUERY_GET_ALL_BOOK;
        Cursor result = db.rawQuery(sql, null);
        try {
            if (result != null && result.moveToFirst()) {
                do {
                    int id = result.getInt(result.getColumnIndex(DBHelper.COLUMN_BOOK_ID));
                    String title = result.getString(result.getColumnIndex(DBHelper.COLUMN_BOOK_TITLE));
                    String cover = result.getString(result.getColumnIndex(DBHelper.COLUMN_BOOK_COVER)).trim();
                    String description = result.getString(result.getColumnIndex(DBHelper.COLUMN_BOOK_DESCRIPTION));
                    Book book = new Book(id, title, cover, description);
                    lstBook.add(book);
                } while (result.moveToNext());
            }
        } finally {
            if (result != null)
                result.close();
        }

        return lstBook;
    }

}
