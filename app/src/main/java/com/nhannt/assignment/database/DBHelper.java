package com.nhannt.assignment.database;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by NhanNT on 04/17/2017.
 */

public class DBHelper extends SQLiteOpenHelper {

    public static final String TABLE_BOOK = "kimdung";
    public static final String COLUMN_BOOK_ID = "stID";
    public static final String COLUMN_BOOK_TITLE = "stName";
    public static final String COLUMN_BOOK_COVER = "stImage";
    public static final String COLUMN_BOOK_DESCRIPTION = "stDescribe";
    public static final String QUERY_GET_ALL_BOOK = "SELECT * FROM kimdung";

    public static final String TABLE_CHAPTER = "st_kimdung";
    public static final String COLUMN_CHAPTER_ID = "deID";
    public static final String COLUMN_CHAPTER_TITLE  = "deName";
    public static final String COLUMN_CHAPTER_SOURCE  = "deSource";
    public static final String COLUMN_CHAPTER_CONTENT  = "decontent";
    public static final String COLUMN_CHAPTER_BOOK_ID  = "stID";
    public static final String COLUMN_CHAPTER_DATE  = "deDate";


    private static String DB_NAME = "kimdung.sqlite";
    private static final int DB_VERSION = 1;
    private static final String DB_PATH_SUFFIX = "/databases/";

    private SQLiteDatabase mDataBase;
    private static Context mContext;

    public DBHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
        this.mContext = context;
    }

    public void openDataBase() throws SQLException {
        File dbFile = mContext.getDatabasePath(DB_NAME);
        if (!dbFile.exists()) {
            try {
                copyDataBaseFromAsset();
                System.out.println("Copying sucess from Assets folder");
            } catch (IOException e) {
                throw new RuntimeException("Error creating source database", e);
            }
        }
    }

    private static String getDatabasePath() {
        return mContext.getApplicationInfo().dataDir + DB_PATH_SUFFIX
                + DB_NAME;
    }

    public void copyDataBaseFromAsset() throws IOException {
        InputStream myInput = mContext.getAssets().open(DB_NAME);
        // Path to the just created empty db
        String outFileName = getDatabasePath();
        // if the path doesn't exist first, create it
        File f = new File(mContext.getApplicationInfo().dataDir + DB_PATH_SUFFIX);
        if (!f.exists())
            f.mkdir();
        // Open the empty db as the output stream
        OutputStream myOutput = new FileOutputStream(outFileName);
        // transfer bytes from the inputfile to the outputfile
        byte[] buffer = new byte[1024];
        int length;
        while ((length = myInput.read(buffer)) > 0) {
            myOutput.write(buffer, 0, length);
        }
        // Close the streams
        myOutput.flush();
        myOutput.close();
        myInput.close();
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }


}
