package com.nhannt.assignment;

import com.nhannt.assignment.utils.StringUtils;

/**
 * Created by NhanNT on 05/07/2017.
 */

public class SearchKeyWord {
    public boolean search(String content, String keywords) {
        String[] keySplit = StringUtils.splitString(keywords);
        String[] contentSplit = StringUtils.splitString(content);
        int mark = 0;
        for (int i = 0; i < keySplit.length; i++) {
            boolean check = false;
            for (int j = mark; j < contentSplit.length; j++) {
                if (contentSplit[j] == null ? keySplit[i] == null : contentSplit[j].equals(keySplit[i])) {
                    mark = j;
                    check = true;
                    break;
                }
            }
            if (!check) {
                return false;
            }
        }
        return true;
    }

//    public String search(String content, String keywords) {
//        if (!contains(content, keywords)) {
//            return null;
//        } else {
//            String[] keySplit = StringUtils.splitString(keywords);
//            String[] contentSplit = StringUtils.splitString(content);
//            String searchString = "";
//            int mark = 0;
//            for (int i = 0; i < keySplit.length; i++) {
//                for (int j = mark; j < contentSplit.length; j++) {
//                    if (contentSplit[j] == null ? keySplit[i] == null : contentSplit[j].equals(keySplit[i])) {
//                        mark = j;
//                        searchString += contentSplit[j] + " " + contentSplit[j + 1] + "...";
//                        break;
//                    }
//                }
//
//            }
//            return searchString;
//        }
//    }
}
