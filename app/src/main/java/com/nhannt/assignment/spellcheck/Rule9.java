package com.nhannt.assignment.spellcheck;

/**
 * Created by NhanNT on 04/30/2017.
 * Nguyên âm "ơ", "ớ", "ờ", "ở", "ợ", "ỡ" thì chỉ có đằng sau là nguyên âm "i" hoặc "u"
 * "ơ" ớ ợ thì có đi với u
 * Nguyên âm "ô", "ố", "ồ", "ổ", "ộ", "ỗ" thì chỉ có đằng sau là nguyên âm "i"
 *
 */
public class Rule9 extends Rule {
    @Override
    public boolean check(String str) {
        String temp = "ỡôốồổộỗ";
        String temp1 = "ơớờởợỡ";
        char[] chars = str.toCharArray();
        for (int i = 0; i < (chars.length - 1); i++) {
            if (temp.contains(chars[i] + "") && isVowel(chars[i+1]) ) {
                if((chars[i + 1] != 'i'))
                    return false;
            }
            if(temp1.contains(chars[i]+"") && isVowel(chars[i+1])){
                if(!(chars[i+1] =='i' || chars[i+1] == 'u'))
                    return false;
            }
        }
        return true;
    }

    @Override
    protected int getId() {
        return 9;
    }
}
