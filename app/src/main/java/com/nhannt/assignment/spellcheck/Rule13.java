package com.nhannt.assignment.spellcheck;

/**
 * Created by NhanNT on 04/30/2017.
 * Các nguyên âm được phép sau nguyên âm "i": a, u, ê, ế, ệ, ểiễ, ề, ữ â ậ ư,....
 *
 *
 */
public class Rule13 extends Rule{
    @Override
    public boolean check(String str) {
        String follow = "aãáàảạũùuúụêếệểễềóốỗọỏòẫậấầâặơờởữưắặăớỡằeẻ";
        char[] chars = str.toCharArray();
        for (int i = 0; i < chars.length - 1; i++) {
            if (chars[i] == 'i' && isVowel(chars[i+1])) {
                if (!follow.contains(chars[i + 1] + "")) {
                    return false;
                }
            }
        }
        return true;
    }


    @Override
    protected int getId() {
        return 13;
    }
}
