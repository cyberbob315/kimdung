package com.nhannt.assignment.spellcheck;

/**
 * Created by NhanNT on 05/01/2017.
 * G, GH - NG, NGH
 * G - NG viết trước nguyên âm a, ă, â, o, ô ơ, u, ư,i,ì
 * GH  viết trước nguyên âm e, ê, i
 */
public class Rule16 extends Rule {

    String arrG_NG = "uúùủụũưứừửựữoóòỏọõôốồổộỗơớờởợỡaáàạảãâấầẩậẫăắằẳặẵễiìỉ";
    String arrGH = "eéèẻẹẽêếềểệễiíìỉịĩ";

    @Override
    public boolean check(String str) {
        char[] chars = str.toCharArray();
        for (int i = 0; i < chars.length - 1; i++) {
            if (chars[i] == 'g') {
                if (isConsonant(chars[i + 1])) {
                    if (chars[i + 1] != 'h')
                        return false;
                } else {
                    if (!arrG_NG.contains(chars[i + 1] + ""))
                        return false;
                }
            }
        }

        for (int i = 0; i < chars.length - 2; i++) {
            if (chars[i] == 'g' && chars[i + 1] == 'h') {
                if (!arrGH.contains(chars[i + 2] + ""))
                    return false;
            }
            if (chars[i] == 'n' && chars[i + 1] == 'g' && isVowel(chars[i + 2]))
                if (!arrG_NG.contains(chars[i + 2] + ""))
                    return false;
        }

        return true;
    }

    @Override
    protected int getId() {
        return 16;
    }
}
