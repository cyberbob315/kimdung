package com.nhannt.assignment.spellcheck;

/**
 * Created by NhanNT on 04/29/2017.
 * Số phụ âm trong 1 từ
 *
 */
public class Rule1 extends Rule {
    @Override
    public boolean check(String str) {
        char[] chars = str.toCharArray();
        int count = 0;
        for (char c : chars) {
            if (isConsonant(c))
                count++;
        }
        if (count <= MAX_CONSONANT && count >= MIN_CONSONANT)
            return true;
        return false;
    }

    @Override
    protected int getId() {
        return 1;
    }
}
