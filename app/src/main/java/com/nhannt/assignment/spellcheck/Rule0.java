package com.nhannt.assignment.spellcheck;

/**
 * Created by NhanNT on 04/30/2017.
 * Phải là chữ cái tiếng viết
 */
public class Rule0 extends Rule {
    @Override
    public boolean check(String str) {
        char[] chars = str.toCharArray();
        for (int i = 0; i <chars.length ; i++) {
            if(!isConsonant(chars[i]) && !isVowel(chars[i]))
                return false;
        }
        return true;
    }

    @Override
    protected int getId() {
        return 0;
    }
}
