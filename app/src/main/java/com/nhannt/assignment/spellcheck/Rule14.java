package com.nhannt.assignment.spellcheck;

/**
 * Created by NhanNT on 04/30/2017.
 * Nguyên âm sau "o" để tạo thành cặp nguyên âm: "a", "i", "e","á,"à",ắ,ặ ằ,....
 * Nếu là oo thì phải là 1 trong 3 từ xoong boong coong
 */
public class Rule14 extends Rule {

    @Override
    public boolean check(String str) {
        String follow = "aieáàảạắặằăẵéẹèẻã";
        char[] chars = str.toCharArray();
        for (int i = 0; i < chars.length - 1; i++) {
            if (chars[i] == 'o' && isVowel(chars[i+1]) && chars[i+1] != 'o') {
                if(!follow.contains(chars[i + 1] + ""))
                    return false;
            }
            if (chars[i] == 'o' && chars[i + 1] == 'o' && !(str.equals("xoong") || str.equals("boong") || str.equals("coong")))
                return false;
        }
        return true;
    }

    @Override
    protected int getId() {
        return 14;
    }
}
