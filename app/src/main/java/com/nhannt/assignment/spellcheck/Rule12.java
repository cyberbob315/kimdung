package com.nhannt.assignment.spellcheck;

/**
 * Created by NhanNT on 04/30/2017.
 * Có một số nguyên âm không được đi sau "a" để tạo thành cặp nguyên âm:
 * tất cả những nguyên âm nào có dấu thì không được đi sau "a", aa, aô, aê, aư, aơ, aă, aâ, ae,
 * Chỉ có một số nguyên âm được phép đứng đằng sau "á" để tạo thành cặp nguyên âm :ái, áu, áo, áy
 * Chỉ có một số nguyên âm được phép đứng đằng sau "à" để tạo thành cặp nguyên âm: ài, àu, ào, ày.
 * Chỉ có một số nguyên âm được phép đứng đằng sau "ã" để tạo thành cặp nguyên âm: ãi, ão, ãy
 * Chỉ có một số nguyên âm được phép đứng đằng sau "ạ" để tạo thành cặp nguyên âm: ại, ạo, ạy
 * Chỉ có một số nguyên âm được phép đứng đằng sau "ả" để tạo thành cặp nguyên âm: ải, ảo, ảy.
 * Nếu có "ảu" thì từ này bắt buộc phải nằm trong từ "nhảu"
 */
public class Rule12 extends Rule {
    String arr1 = "a á à ạ ả ã ô ố ồ ổ ỗ ộ ư ứ ừ ử ữ ự ơ ớ ờ ở ỡ ợ ă ắ ằ ẳ ặ ẵ â ấ ầ ẩ ẫ ậ e é è ẻ ẽ ẹ";
    String arr2 = "ioy";
    String arr3 = "iuoy";

    @Override
    public boolean check(String str) {
        String temp = "ãạả";
        String temp2 = "áà";
        char[] chars = str.toCharArray();
        for (int i = 0; i < (chars.length - 1); i++) {
            if ((chars[i] == 'a') && arr1.contains(chars[i + 1] + ""))
                return false;
            if (temp.contains(chars[i] + "") && isVowel(chars[i + 1]))
                if (!arr2.contains(chars[i + 1] + ""))
                    return false;

            if (temp2.contains(chars[i] + "") && isVowel(chars[i + 1]))
                if (!arr3.contains(chars[i + 1] + ""))
                    return false;
            if ((chars[i] == 'ả') && (chars[i + 1] == 'u')) {
                if (!str.equals("nhảu"))
                    return false;
            }

        }
        return true;
    }

    @Override
    protected int getId() {
        return 12;
    }
}
