package com.nhannt.assignment.spellcheck;


/**
 * Created by NhanNT on 04/29/2017.
 * Nếu có 3 phụ âm đứng cạnh nhau thì phải là ngh
 * NGH viết trước nguyên âm e, ê, i
 */
public class Rule6 extends Rule {
    String arr = "e é è ẻ ẹ ẽ ê ế ề ể ễ ệ i í ì ỉ ị ĩ";

    @Override
    public boolean check(String str) {
        char[] chars = str.toCharArray();
        for (int i = 1; i < chars.length - 1; i++) {
            if (isConsonant(chars[i - 1]) && isConsonant(chars[i]) && isConsonant(chars[i + 1])) {
                String temp = Character.toString(chars[i-1]) + Character.toString(chars[i]) + Character.toString(chars[i+1]);
                if (!temp.equals("ngh"))
                    return false;
            }
        }

        if (chars.length >= 4) {
            if (isConsonant(chars[0]) && isConsonant(chars[1]) && isConsonant(chars[2])) {
                String temp = Character.toString(chars[0]) + Character.toString(chars[1]) + Character.toString(chars[2]);
//                System.out.println(temp);
                if (temp.equals("ngh")) {
                    if (!arr.contains(chars[3] + ""))
                        return false;
                } else
                    return false;
            }
        }

        return true;
    }

    @Override
    protected int getId() {
        return 6;
    }
}
