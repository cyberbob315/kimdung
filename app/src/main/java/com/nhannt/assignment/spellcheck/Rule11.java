package com.nhannt.assignment.spellcheck;

/**
 * Created by NhanNT on 04/30/2017.
 * Chỉ có một số nguyên âm được phép đứng đằng sau "â" để tạo thành cặp nguyên âm: âu, ây
 * Chỉ có một số nguyên âm được phép đứng đằng sau "ấ" để tạo thành cặp nguyên âm: ấu, ấy
 * Chỉ có một số nguyên âm được phép đứng đằng sau "ẩ" để tạo thành cặp nguyên âm: ẩu, ẩy
 * Chỉ có một số nguyên âm được phép đứng đằng sau "ẫ" để tạo thành cặp nguyên âm: ẫu, ẫy
 * Chỉ có một số nguyên âm được phép đứng đằng sau "ậ" để tạo thành cặp nguyên âm: ậu, ậy
 * Chỉ có một số nguyên âm được phép đứng đằng sau "ầ" để tạo thành cặp nguyên âm: ầu, ầy
 */
public class Rule11 extends Rule {
    @Override
    public boolean check(String str) {
        String temp = "âấẩẫậầ";
        String temp2 = "uy";
        char[] chars = str.toCharArray();
        for(int i =0;i<(chars.length-1);i++){
            if(temp.contains(chars[i]+"") && isVowel(chars[i+1]))
                if(!temp2.contains(chars[i+1] +""))
                return false;
        }
        return true;
    }

    @Override
    protected int getId() {
        return 11;
    }
}
