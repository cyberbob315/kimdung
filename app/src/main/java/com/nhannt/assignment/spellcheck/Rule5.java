package com.nhannt.assignment.spellcheck;

/**
 * Created by NhanNT on 04/29/2017.
 * phụ âm không được đứng cuối từ: q v b d l k s x r đ đ
 *
 */
public class Rule5 extends Rule {
    private static final String NOT_LAST_CONSONANT = "qvbdlksxrđđ";

    @Override
    public boolean check(String str) {
        if (NOT_LAST_CONSONANT.contains(str.charAt(str.length() -1 ) + "")) {
            return false;
        }
        return true;
    }

    @Override
    protected int getId() {
        return 5;
    }
}
