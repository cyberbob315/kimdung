package com.nhannt.assignment.spellcheck;

/**
 * Created by NhanNT on 04/29/2017.
 * 1 phụ âm không được đứng giữa 2 nguyên âm
 */
public class Rule4 extends Rule {
    @Override
    public boolean check(String str) {
        char[] chars = str.toCharArray();
        for(int i = 1;(i<chars.length-1);i++){
            if(isConsonant(chars[i]) && isVowel(chars[i-1]) && isVowel(chars[i+1])){
                return false;
            }
        }
        return true;
    }

    @Override
    protected int getId() {
        return 4;
    }
}
