package com.nhannt.assignment.spellcheck;

import java.util.ArrayList;

/**
 * Created by NhanNT on 04/22/2017.
 */
public class SpellChecker {
    private static SpellChecker mInstance;
    private ArrayList<Rule> lstRule;

    public static SpellChecker getInstance() {
        if (mInstance == null)
            mInstance = new SpellChecker();
        return mInstance;
    }

    private boolean isNumber(String word) {
        char[] chars = word.toCharArray();
        for (char c : chars)
            if (!Character.isDigit(c))
                return false;
        return true;
    }

    private SpellChecker() {
        lstRule = new ArrayList<>();
        lstRule.add(new Rule0());
        lstRule.add(new Rule1());
        lstRule.add(new Rule2());
        lstRule.add(new Rule3());
        lstRule.add(new Rule4());
        lstRule.add(new Rule5());
        lstRule.add(new Rule6());
        lstRule.add(new Rule7());
        lstRule.add(new Rule8());
        lstRule.add(new Rule9());
        lstRule.add(new Rule10());
        lstRule.add(new Rule11());
        lstRule.add(new Rule12());
        lstRule.add(new Rule13());
        lstRule.add(new Rule14());
        lstRule.add(new Rule15());
        lstRule.add(new Rule16());
    }

    public boolean check(String word) {
        if (word == null || word.isEmpty()) return true;
        if (isNumber(word)) return true;
        if (word.contains("Ð")) {
            word = word.replaceAll("Ð", "Đ");
        }
        word = word.toLowerCase();
        for (Rule rule : lstRule) {
            if (!rule.check(word)) {
                return false;
            }
        }
        return true;
    }
}
