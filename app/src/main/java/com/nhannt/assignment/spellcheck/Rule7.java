package com.nhannt.assignment.spellcheck;

/**
 * Created by NhanNT on 04/29/2017.
 * KHÔNG nguyên âm được phép đứng đằng sau "ă","ằ", "ẵ", "ẳ", "ặ" để tạo thành cặp nguyên âm.
 */
public class Rule7 extends Rule {

    @Override
    public boolean check(String str) {
        String array = "ăằẵẳặ";
        char[] chars = str.toCharArray();
        for (int i = 0; i < (chars.length - 1); i++) {
            if (array.contains(chars[i] + "") && isVowel(chars[i + 1])) {
                return false;
            }
        }
        return true;
    }

    @Override
    protected int getId() {
        return 7;
    }
}
