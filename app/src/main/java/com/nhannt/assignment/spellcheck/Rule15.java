package com.nhannt.assignment.spellcheck;

/**
 * Created by NhanNT on 05/01/2017.
 * K viết trước nguyên âm e, ê, i, y nếu là phụ âm thì phải là h
 *C viết trước nguyên âm đơn khác như a, ă, â, o, ô, ơ, u, ư nếu là phụ âm thì phải là h
 *Q viết trước nguyên âm u và ko đứng trước nguyên âm
 */
public class Rule15 extends Rule {

    String arrK ="eéẻèẽẹêếểềệễiíìỉịĩỹỵýỳỷ";
    String arrC ="uúùủụũưứừửựữoóòỏọõôốồổộỗơớờởợỡeéèẻẹẽêếềểệaáàạảãâấầẩậẫăắằẳặẵễ";

    @Override
    public boolean check(String str) {

        if(str.charAt(0) == 'k'){
            if(isConsonant(str.charAt(1))) {
                if (str.charAt(1) != 'h')
                    return false;
            }else{
                if(!arrK.contains(str.charAt(1)+""))
                    return false;
            }
        }
        if(str.charAt(0) =='c'){
            if(isConsonant(str.charAt(1))) {
                if (str.charAt(1) != 'h')
                    return false;
            }else{
                if(!arrC.contains(str.charAt(1)+""))
                    return false;
            }
        }

        if(str.charAt(0) =='q'){
            if(isConsonant(str.charAt(1))) {
                return false;
            }else{
                if(str.charAt(1) != 'u')
                    return false;
            }
        }
        return true;

    }

    @Override
    protected int getId() {
        return 15;
    }
}
