package com.nhannt.assignment.spellcheck;

/**
 * Created by nhannt on 27/02/2017.
 */
public abstract class Rule {
    public static final String CONSONANT = "bcdđghklmnpqrstvx";
    public static final String VOWEL = "uúùủụũưứừửựữyýỳỷỵỹoóòỏọõôốồổộỗơớờởợỡeéèẻẹẽêếềểệaáàạảãâấầẩậẫăắằẳặẵễiíìỉịĩ";
    public static final int MAX_CONSONANT = 5;
    public static final int MIN_CONSONANT = 0;
    public static final int MAX_VOWEL = 3;
    public static final int MIN_VOWEL = 1;
    public static final String REGEX = "[\\s\\.\\,]+";



    protected boolean isConsonant(char s) {
        String check = s+"";
        if (CONSONANT.contains(check))
            return true;
        return false;
    }

    protected boolean isVowel(char s){
        String check = s+"";
        if(VOWEL.contains(check))
            return true;
        return false;
    }


    protected int id = getId();

    public void show() {
        System.out.println("Từ này sai luật số :" + id);
    }

    public abstract boolean check(String str);

    protected abstract int getId();
}
