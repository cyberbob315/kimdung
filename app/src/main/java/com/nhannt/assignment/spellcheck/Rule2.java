package com.nhannt.assignment.spellcheck;

/**
 * Created by NhanNT on 04/29/2017.
 * Số nguyên âm trong 1 từ
 */
public class Rule2 extends Rule {
    @Override
    public boolean check(String str) {
        char[] chars = str.toCharArray();
        int count = 0;
        for (char c : chars) {
            if (isVowel(c))
                count++;
        }
        if (count <= MAX_VOWEL && count >= MIN_VOWEL)
            return true;
        return false;
    }

    @Override
    protected int getId() {
        return 2;
    }
}
