package com.nhannt.assignment.spellcheck;

/**
 * Created by NhanNT on 04/30/2017.
 * Nguyên âm "u" đi đằng sau sẽ là các nguyên âm "i", "a", "y", "ê", "ở", "ế","é","è","ẻ","ệ","â","ô","ồ","ố",ổ,ộ,ỗ,ậ,ấ,....
 *
 * Nguyên âm "ũ,ú,ù,ủ,ụ" thì đi đằng sau nếu là nguyên âm thì phải là "a,i,y"
 */
public class Rule10 extends Rule {
    @Override
    public boolean check(String str) {
        String temp2 = "ịíìiỉayêởếéèẻẹệâấậầâẫẩôồổốỗộậấảýỹeơáảạàỷỵỳẳẵăắặằãểếề";
        String temp = "ũúùủụ";
        char[] chars = str.toCharArray();
        for (int i = 0; i < (chars.length - 1); i++) {
            if (chars[i] == 'u' && isVowel(chars[i + 1])) {
                if (!temp2.contains(chars[i + 1] + "")) {
                    return false;
                }

            }
            if (temp.contains(chars[i] + "") && isVowel(chars[i + 1]))
                if (!(chars[i + 1] == 'a' || chars[i + 1] == 'i' || chars[i+1] =='y'))
                    return false;
        }
        return true;
    }

    @Override
    protected int getId() {
        return 10;
    }
}
