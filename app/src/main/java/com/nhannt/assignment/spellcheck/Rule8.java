package com.nhannt.assignment.spellcheck;

/**
 * Created by NhanNT on 04/30/2017.
 * Có những phụ âm đứng cuối nhưng trước nó không phải là phụ âm: n m t p c
 * Nếu phụ âm đứng cuối là h thì trước đó phải là n hoặc c
 * Nếu phụ âm đứng cuối là g thì trước đo phải là n
 */
public class Rule8 extends Rule {
    @Override
    public boolean check(String str) {
        String arr = "nmptc";
        if (arr.contains(str.charAt(str.length() - 1) + "") && isConsonant(str.charAt(str.length() - 2)))
            return false;
        if(str.charAt(str.length() - 1) == 'g' && str.charAt(str.length()-2) != 'n')
            return false;
        if(str.charAt(str.length() - 1) == 'h' && !(str.charAt(str.length()-2) == 'n' || str.charAt(str.length()-2) == 'c'))
            return false;
        return true;
    }

    @Override
    protected int getId() {
        return 8;
    }
}
