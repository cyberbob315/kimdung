package com.nhannt.assignment.spellcheck;

/**
 * Created by NhanNT on 04/29/2017.
 * Các nguyên âm được phép sau "e", "é", "è", "ẻ", "ẹ", "ẽ" thì chỉ có: "o"
 */
public class Rule3 extends Rule {


    @Override
    public boolean check(String str) {
        String temp = "eéèẻẹẽ";
        char[] chars = str.toCharArray();
        for (int i = 0; i < (chars.length - 1); i++) {
            if (temp.contains(chars[i] + "") && isVowel(chars[i+1])) {
                if((chars[i + 1] != 'o'))
                return false;
            }
        }
        return true;
    }

    @Override
    protected int getId() {
        return 3;
    }
}
