package com.nhannt.assignment.utils;

import android.text.Html;

/**
 * Created by NhanNT on 04/24/2017.
 */

public class StringUtils {
    public static String[] splitString(String source){
        return source.split("[\\s*|[,]*|[(]*|[']*|[)]*|[\\\\.]*|[:]*|[/]*|[!]*|[?]*|[+]*|[-]*|[\"]*|[;]*|\\[*|\\]*|[0-9]*]+");
    }

    public static int countWord(String source){
        String[] arr = splitString(source);
        int count = 0;
        for(String str : arr){
            count++;
        }
        return count;
    }

    public static String convertHtmlToString(String htmlSource){
        String result;
        if (Common.isNougat())
            result = Html.fromHtml(htmlSource, Html.FROM_HTML_MODE_LEGACY).toString();
        else
            result = Html.fromHtml(htmlSource).toString();
        return result;
    }

    public static String getFirst2Words(String source){
        String result ="";
        String[] arr = null;
        if(countWord(source)>=2) {
            arr = splitString(source);
            return arr[0] + " " + arr[1];
        }
        return null;
    }

    public static String getKeyWordOfSearch(String source){
        String result ="";
        String[] arr=null;
        if(countWord(source)>2){
            arr = splitString(source);
            StringBuilder builder = new StringBuilder();
            for(int i =2 ;i<arr.length;i++) {
                builder.append(arr[i]);
                builder.append(" ");
            }
            return builder.toString();
        }
        return null;
    }
}
