package com.nhannt.assignment.base;

import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.speech.RecognizerIntent;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.miguelcatalan.materialsearchview.MaterialSearchView;
import com.nhannt.assignment.R;
import com.nhannt.assignment.utils.StringUtils;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by NhanNT on 04/23/2017.
 */

public abstract class BaseActivity extends AppCompatActivity  {

    protected MaterialSearchView searchView;
    protected ProgressDialog mProgressDialog;
    private final int REQ_CODE_SPEECH_INPUT = 100;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState, @Nullable PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
        setContentView(getLayout());
//        searchView = (MaterialSearchView) findViewById(R.id.search_view);
//        searchView.setOnQueryTextListener(this);
    }

    protected abstract int getLayout();

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(getMenuLayoutId(), menu);

        MenuItem item = menu.findItem(R.id.action_search);
        searchView.setMenuItem(item);
        return super.onCreateOptionsMenu(menu);
    }

    protected abstract int getMenuLayoutId();

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_voice_command:
                promptSpeechInput();
                break;
            case R.id.action_spell_check:
                triggerSpellCheck();
                break;
        }
        return true;
    }

    protected void share(String title, String body) {
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, title);
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, body);
        startActivity(Intent.createChooser(sharingIntent, getString(R.string.share_using)));
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode){
            case REQ_CODE_SPEECH_INPUT:
                if (resultCode == RESULT_OK && null != data) {

                    ArrayList<String> resultArr = data
                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    String result = resultArr.get(0);
                    result = result.toLowerCase();
                    Toast.makeText(this, result, Toast.LENGTH_SHORT).show();
                    if(StringUtils.countWord(result) > 2){
                        String first2Words = StringUtils.getFirst2Words(result);
                        if(first2Words.equals("tìm kiếm")){
                            triggerSearchView(StringUtils.getKeyWordOfSearch(result));
                            return;
                        }
                    }else{
                        switch (result.toLowerCase()){
                            case "tìm kiếm":
                                triggerSearchView(null);
                                return;
                            case "chính tả":
                                triggerSpellCheck();
                                return;
                        }
                    }
                    showNotiDialog(getString(R.string.command_not_supported));
                }
                break;
        }
    }

    protected void showNotiDialog(String text){
        AlertDialog alertDialog = new AlertDialog.Builder(this)
                .setMessage(text)
                .setPositiveButton(getString(R.string.btn_ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .create();
        alertDialog.show();
    }
    protected abstract void triggerSearchView(String keyword);
    protected abstract void triggerSpellCheck();

    @Override
    public void onBackPressed() {
        if (searchView.isSearchOpen()) {
            searchView.closeSearch();
        } else {
            super.onBackPressed();
        }
    }
    private void promptSpeechInput() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
                getString(R.string.speech_prompt));
        try {
            startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);
        } catch (ActivityNotFoundException a) {
            Toast.makeText(getApplicationContext(),
                    getString(R.string.speech_not_supported),
                    Toast.LENGTH_SHORT).show();
        }
    }

    protected void showProgressDialog(String message) {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage(message);
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setCancelable(false);
        }
        mProgressDialog.show();
    }

    protected void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.hide();
            mProgressDialog.dismiss();
        }
    }
}
