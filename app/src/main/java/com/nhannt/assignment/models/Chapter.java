package com.nhannt.assignment.models;

/**
 * Created by NhanNT on 04/19/2017.
 */

public class Chapter {
    private int id;
    private int bookId;
    private String title;
    private String content;
    private String date;

    public Chapter() {
    }

    public Chapter(int id, int bookId, String title, String content, String date) {
        this.id = id;
        this.bookId = bookId;
        this.title = title;
        this.content = content;
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getBookId() {
        return bookId;
    }

    public void setBookId(int bookId) {
        this.bookId = bookId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
