package com.nhannt.assignment.models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by NhanNT on 04/17/2017.
 */

public class Book implements Serializable {

    private int id;
    private String title;
    private String cover;
    private String description;
    private ArrayList<Chapter> lstChapter;

    public Book() {
    }

    public Book(int id, String title, String cover, String description) {
        this.id = id;
        this.title = title;
        this.cover = cover;
        this.description = description;

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ArrayList<Chapter> getLstChapter() {
        return lstChapter;
    }

    public void setLstChapter(ArrayList<Chapter> lstChapter) {
        this.lstChapter = lstChapter;
    }
    public void addChapter(Chapter chapter){
        if(this.lstChapter == null)
            lstChapter = new ArrayList<>();
        lstChapter.add(chapter);
    }
}
