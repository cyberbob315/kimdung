package com.nhannt.assignment.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nhannt.assignment.R;
import com.nhannt.assignment.activities.ChapterDetailActivity;
import com.nhannt.assignment.models.Chapter;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by NhanNT on 04/19/2017.
 */

public class ChapterAdapter extends RecyclerView.Adapter<ChapterAdapter.ChapterViewHolder> {

    private Context mContext;
    private ArrayList<Chapter> mData;
    private LayoutInflater mLayoutInflater;

    public ChapterAdapter(Context mContext, ArrayList<Chapter> mData) {
        this.mContext = mContext;
        this.mData = mData;
        mLayoutInflater = LayoutInflater.from(mContext);
    }

    @Override
    public ChapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mLayoutInflater.inflate(R.layout.item_chapter, parent, false);
        ChapterViewHolder holder = new ChapterViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(ChapterViewHolder holder, int position) {
        holder.tvTitle.setText(mData.get(position).getTitle());
        holder.pos = position;
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public class ChapterViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        int pos;
        @BindView(R.id.tv_chapter_title_item)
        TextView tvTitle;

        public ChapterViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(mContext, ChapterDetailActivity.class);
            intent.putExtra(ChapterDetailActivity.KEY_CHAPTER_ID, mData.get(pos).getId());
            intent.putExtra(ChapterDetailActivity.KEY_CHAPTER_POS, pos);
            intent.putExtra(ChapterDetailActivity.KEY_IS_SEARCH_RESULT,false);
            mContext.startActivity(intent);
        }
    }
}
