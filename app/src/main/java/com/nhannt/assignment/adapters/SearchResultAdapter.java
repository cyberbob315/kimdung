package com.nhannt.assignment.adapters;

import android.content.Context;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.nhannt.assignment.R;
import com.nhannt.assignment.models.Book;
import com.nhannt.assignment.utils.Common;
import com.nhannt.assignment.widgets.SquareImageView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by NhanNT on 04/23/2017.
 */

public class SearchResultAdapter extends BaseExpandableListAdapter {

    private Context mContext;
    private ArrayList<Book> mData;
    private LayoutInflater mLayoutInflater;

    public SearchResultAdapter(Context mContext, ArrayList<Book> mData) {
        this.mContext = mContext;
        this.mData = mData;
        mLayoutInflater = LayoutInflater.from(mContext);
    }

    @Override
    public int getGroupCount() {
        return mData.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return mData.get(groupPosition).getLstChapter().size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return mData.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return mData.get(groupPosition).getLstChapter().get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return mData.get(groupPosition).getId();
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return mData.get(groupPosition).getLstChapter().get(childPosition).getId();
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        GroupViewHolder holder;
        Book item = mData.get(groupPosition);
        if(convertView == null){
            convertView = mLayoutInflater.inflate(R.layout.item_book_linear, parent, false);
            holder = new GroupViewHolder(convertView);
            convertView.setTag(holder);
        }else{
            holder = (GroupViewHolder) convertView.getTag();
        }
        holder.tvBookTitle.setText(item.getTitle());
        int coverId = Common.getResId(item.getCover(),R.drawable.class);
        Glide.with(mContext)
                .load(coverId)
                .placeholder(R.mipmap.ic_launcher)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.ivBookCover);
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        ChildViewHolder holder;
        if(convertView == null){
            convertView = mLayoutInflater.inflate(R.layout.item_chapter_expand, parent, false);
            holder = new ChildViewHolder(convertView);
            convertView.setTag(holder);
        }else {
            holder = (ChildViewHolder) convertView.getTag();
        }
        holder.tvTitle.setText(mData.get(groupPosition).getLstChapter().get(childPosition).getTitle());
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    public class ChildViewHolder{

        @BindView(R.id.tv_chapter_title_item)
        TextView tvTitle;

        public ChildViewHolder(View itemView) {
            ButterKnife.bind(this,itemView);
        }
    }

    public class GroupViewHolder{
        @BindView(R.id.book_title_item)
        AppCompatTextView tvBookTitle;
        @BindView(R.id.book_cover_item)
        SquareImageView ivBookCover;

        public GroupViewHolder(View itemView) {
            ButterKnife.bind(this,itemView);
        }
    }
}
