package com.nhannt.assignment.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.nhannt.assignment.R;
import com.nhannt.assignment.activities.ChapterActivity;
import com.nhannt.assignment.models.Book;
import com.nhannt.assignment.utils.Common;
import com.nhannt.assignment.widgets.SquareImageView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by NhanNT on 04/17/2017.
 */

public class BookAdapter extends RecyclerView.Adapter<BookAdapter.ViewHolder> {

    private Context mContext;
    private ArrayList<Book> mData;
    private LayoutInflater mLayoutInflater;

    public BookAdapter(Context mContext, ArrayList<Book> mData) {
        this.mContext = mContext;
        this.mData = mData;
        mLayoutInflater = LayoutInflater.from(mContext);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mLayoutInflater.inflate(R.layout.item_book_grid, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Book item = mData.get(position);
        holder.position = position;
        holder.tvBookTitle.setText(item.getTitle());
        int coverId = Common.getResId(item.getCover(),R.drawable.class);
        Glide.with(mContext)
                .load(coverId)
                .placeholder(R.mipmap.ic_launcher)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.ivBookCover);
        if (Common.isMarshMallow()) {
            holder.ivBookCover.setTransitionName("transition_image" + position);
        }
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        int position;
        @BindView(R.id.book_title_item)
        AppCompatTextView tvBookTitle;
        @BindView(R.id.book_cover_item)
        SquareImageView ivBookCover;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(mContext, ChapterActivity.class);
            intent.putExtra(ChapterActivity.KEY_BOOK, mData.get(position).getId());
            if(Common.isMarshMallow()) {
                intent.putExtra(ChapterActivity.KEY_TRANSITION_NAME, ivBookCover.getTransitionName());
                intent.putExtra(ChapterActivity.KEY_IS_USE_TRANSITION, true);
                ActivityOptionsCompat options = ActivityOptionsCompat.
                        makeSceneTransitionAnimation((AppCompatActivity)mContext, ivBookCover, ivBookCover.getTransitionName());
                mContext.startActivity(intent,options.toBundle());

            }else{
                intent.putExtra(ChapterActivity.KEY_IS_USE_TRANSITION, false);
                mContext.startActivity(intent);
            }
        }
    }
}
